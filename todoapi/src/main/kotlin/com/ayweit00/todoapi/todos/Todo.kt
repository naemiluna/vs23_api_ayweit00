package com.ayweit00.vs23_api_ayweit00

import jakarta.persistence.*

@Entity
@Table(name = "tb_todos")
data class TodoItem (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long = 0,
        val todo: String,
        val priority: Int = 2
)