package com.ayweit00.vs23_api_ayweit00

import org.springframework.data.repository.CrudRepository

interface TodoRepository : CrudRepository<TodoItem, Long>
