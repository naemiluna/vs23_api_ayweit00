package com.ayweit00.vs23_api_ayweit00

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.net.ResponseCache

@Service
class TodoService(@Autowired private val repository: TodoRepository) {
    fun getAll(): List<TodoItem> = repository.findAll().toList()

    fun getById(id: Long): TodoItem = repository.findByIdOrNull(id) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)

    fun create(todoItem: TodoItem): TodoItem = repository.save(todoItem)

    fun deleteById(id: Long) {
        if (repository.existsById(id)) {
            repository.deleteById(id)
        } else {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }
    }
}
